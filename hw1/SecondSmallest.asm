.data  
#example input  
noSMin: .asciiz "No second smallest"
values:  .word   8,8,8,8,8,8#array contents
size:    .word 6 #size of the array
# Your code will be tested with various inputs
.text
#write your code below

main:	la $a0 values       #$a0=load address of values array
		lw $a1 size       #$a1= size of array
    	
    	jal definition  #call definition func.
    	
    	
    	
definition:
    li $t0 0       		#counter = 0
    li $t1 0       		#location of array = 0
    sub $a2 $a1 1
    mul $t2 $a2 4
    li $t3 2147483647		#$t3=max int  for min
    lw $t4 values($t2)		#$t4=first element of array	for secondMin
    
    
loop:
	bge $t0 $a1 final 	#if  $t0 >= $a1 then go to final
	lw $a0 values($t1) 		#$a0 = values[i]
	slt $t7 $a0 $t3 	 #if values[i] < min
	beq $t7 1 newMin	#go to newMin
	beq $a0 $t3 updateCounter	#if values[i] == min then dont check (values[i]<sMin)  go to updateCounter
	slt $t7 $a0 $t3 	 #if values[i] < min
	beq $t7 0 sMin		#go to sMin then check ? values[i]<sMin
    
    
newMin: 
	move $t4 $t3		#Smin = min
	move $t3 $a0		#Smin = values[i]
	j updateCounter		#go to updateCounter
    
sMin:
	slt $t7 $a0 $t4 	 	#if values[i] < sMin
	beq $t7 0 updateCounter	#if negative go to updateCounter
	move $t4 $a0			#if positive then sMin=values[i]
    j updateCounter			#go to updateCounter
    
updateCounter:
    addi $t1 $t1 4    	#Every 4 bytes there is an integer in the array
    addi $t0 $t0 1    	#counter ++
    b loop       		#goto loop
    
final:  
	
	beq $t4 2147483647 writeNo
	bne $t4 2147483647 writeSMin
		
writeSMin:
	li $v0 1		#integer print
	move $a0 $t4	#load the min to be printed
	syscall			#execute the print
	li $v0 10      #exit program   
    syscall 

writeNo:
	la $a0, noSMin   	#load a space:  " "
    li $v0, 4       	#print string               
    syscall
    li $v0 10      #exit program   
    syscall 


#if you find the second smallest print it 
#if all the numbers in the array are the same print "No second smallest"
